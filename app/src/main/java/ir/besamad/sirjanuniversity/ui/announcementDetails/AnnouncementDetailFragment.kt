package ir.besamad.sirjanuniversity.ui.announcementDetails

import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Announcement
import ir.besamad.sirjanuniversity.databinding.AnnouncementDetailFragmentBinding
import kotlinx.android.synthetic.main.announcement_detail_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AnnouncementDetailFragment : Fragment(), KodeinAware {

    override val kodein by kodein()
    private val factory: AnnouncementDetailsModelViewFactory by instance<AnnouncementDetailsModelViewFactory>()
    private lateinit var viewModel: AnnouncementDetailViewModel

    lateinit var announcement: Announcement



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        announcement = requireArguments().getParcelable("announcement")!!
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding: AnnouncementDetailFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.announcement_detail_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(AnnouncementDetailViewModel::class.java)
        viewModel.setAnnouncement(announcement)
        binding.announcement = viewModel.announcement.value
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        clickBack()
        openLink()
        setFont()
    }

    fun clickBack() {
        button_announcementsDetailBack_toolBar.setOnClickListener {
            requireActivity().onBackPressed()

        }
    }

    fun openLink() {
        button_link_announcementDetail.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(viewModel.announcement.value?.filePath)
            startActivity(openURL)
        }
    }

    fun setFont() {
        val typeface = Typeface.createFromAsset(requireContext().assets, "fonts/Vazir-FD-WOL.ttf")
        textView_annoucementDetails_toolbar.typeface = typeface
        textView_postedDate_Announcement_Detail.typeface = typeface
        textView_title_Announcement_Detail.typeface = typeface
        textView_expireDate_announcement_Detail.typeface = typeface
        textView_location_announcement_Detail.typeface = typeface
        textView_content_announcementDetail.typeface = typeface
        button_link_announcementDetail.typeface = typeface
    }
}
