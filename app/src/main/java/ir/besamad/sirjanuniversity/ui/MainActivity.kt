package ir.besamad.sirjanuniversity.ui

import android.graphics.Rect
import android.graphics.Typeface
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.MediaController
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomsheet.BottomSheetBehavior
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Discipline
import ir.besamad.sirjanuniversity.data.service.ForegroundService
import ir.besamad.sirjanuniversity.ui.disciplines.DisciplineListFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.discipline_details_fragment.*
import org.jsoup.Jsoup

class MainActivity : AppCompatActivity(), DisciplineListFragment.OnDisciplineChosenListener {

    lateinit var navController: NavController

    var bottomSheetBehavior: BottomSheetBehavior<*>? = null

    // Player declarations
    var playbackPosition = 0
    private var rtspUrl = ""
    private lateinit var mediaController: MediaController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ForegroundService.stopService(this)
        ForegroundService.startService(this, "IAU Service is running...")

        navController = Navigation.findNavController(this, R.id.fragment)
        bottomNav.setupWithNavController(navController)

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
        setBottomSheetAndCallBackBottomSheetBehaviour()

        bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN

        imageView_quit_disciplineClose.setOnClickListener {
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        }
        imageView_quit_disciplineCloseAlt.setOnClickListener {
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
        }

        mediaController = MediaController(this)
    }

    private fun setBottomSheetAndCallBackBottomSheetBehaviour() {
        bottomSheetBehavior?.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
//                Toast.makeText(applicationContext, slideOffset.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    videoView.stopPlayback()
                    videoView.suspend()
                    mediaController.hide()
                }
            }
        })
    }

//    override fun onBackPressed() {
//        if (bottomSheetBehavior?.getState() == BottomSheetBehavior.STATE_EXPANDED) {
//            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
//            return
//        }
//    }

    override fun onBackPressed() {
        if (bottomSheetBehavior?.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
            return
        }
        super.onBackPressed()
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            if (bottomSheetBehavior?.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                val outRect = Rect()
                bottomSheet.getGlobalVisibleRect(outRect)
                if (!outRect.contains(
                        event.rawX.toInt(),
                        event.rawY.toInt()
                    )
                ) bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_HIDDEN)
            }
        }
        return super.dispatchTouchEvent(event)
    }


    fun setData(discipline: Discipline) {
        textView_departmentTitle.text = discipline.departmentTitle
        textView_disciplineTitle.text = discipline.deciplineTitle
        textView_graduationLevel_disciplineDetails.text = discipline.graduationLevel
        textView_description.text = discipline.helpDescription
        textView_description2.text = discipline.videoDescription
        if (!discipline.videoPath.isNullOrEmpty()) {
//        rtspUrl = "http://10.0.2.2:5000/" + discipline.videoPath
            rtspUrl = "http://80.191.169.42/" + discipline.videoPath
        } else {
            videoView.visibility = View.GONE
            cardView_videoView_container.visibility = View.GONE
            cardView_quit_disciplineCloseAlt.visibility = View.VISIBLE

            mediaController.hide()
        }

    }

    fun getFont(): Typeface {
        return Typeface.createFromAsset(this.assets, "fonts/Vazir-FD-WOL.ttf")
    }

    fun setFont() {
        val typeface = getFont()
        textView_departmentTitle.typeface = typeface
        textView_graduationLevel_disciplineDetails.typeface = typeface
        textView_description.typeface = typeface
        textView_description2.typeface = typeface
        textView_disciplineTitle.typeface = typeface
    }


//    fun stripHtml(html: String?): String {
//        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString()
//        } else {
//            Html.fromHtml(html).toString()
//        }
//    }

    fun html2text(html: String?): String {
        if (html == null) {
            return ""
        }
        return Jsoup.parse(html).text()
    }

    fun playVideo() {
        if (videoView.visibility == View.VISIBLE) {
            mediaController.setAnchorView(cardView_videoView_container)
            videoView.setMediaController(mediaController)
            val uri = Uri.parse(rtspUrl)
            videoView.setVideoURI(uri)
            videoView.seekTo(playbackPosition)
            videoView.start()

            videoView.setOnPreparedListener {

            }

            videoView.setOnInfoListener { player, what, extras ->
                if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                    loadingProgressSpinner.visibility = View.INVISIBLE
                }
                true
            }
        }

    }

    override fun onDisciplineChosen(discipline: Discipline) {
        bottomSheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)
        discipline.helpDescription = html2text(discipline.helpDescription)
        discipline.videoDescription = html2text(discipline.videoDescription)
        setData(discipline)
        setFont()
        playVideo()
    }
}
