package ir.besamad.sirjanuniversity.ui.disciplines

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Faculty
import ir.besamad.sirjanuniversity.databinding.RecyclerviewFacultyCellBinding

class FacultyRecyclerAdapter(
    private val faculties: List<Faculty>,
    private val context: Context
): RecyclerView.Adapter<FacultyRecyclerAdapter.FacultyViewHolder>() {


    override fun getItemCount() = faculties.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        FacultyViewHolder(
            DataBindingUtil.inflate<RecyclerviewFacultyCellBinding>(
                LayoutInflater.from(parent.context),
                R.layout.recyclerview_faculty_cell,
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: FacultyViewHolder, position: Int) {
        holder.recyclerViewFacultyCellBinding.faculty = faculties[position]

        val typeface = Typeface.createFromAsset(context.assets, "fonts/Vazir-Medium-FD-WOL.ttf")

//        holder.recyclerViewFacultyCellBinding.textViewFacultyTitleRecyclerViewFacultyCell.setTypeface(typeface)
    }


    fun setTypeface(holder: FacultyViewHolder) {
        val typeface = Typeface.createFromAsset(context.assets, "fonts/Vazir-Medium-FD-WOL.ttf")
    }

    inner class FacultyViewHolder(
        val recyclerViewFacultyCellBinding: RecyclerviewFacultyCellBinding
    ): RecyclerView.ViewHolder(recyclerViewFacultyCellBinding.root)

}