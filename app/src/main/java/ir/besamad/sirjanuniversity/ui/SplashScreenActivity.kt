package ir.besamad.sirjanuniversity.ui

import android.content.Intent
import android.graphics.Typeface
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.ui.announcements.AnnouncementsViewModel
import ir.besamad.sirjanuniversity.ui.announcements.AnnouncementsViewModelFactory
import kotlinx.android.synthetic.main.activity_splash_screen.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class SplashScreenActivity : AppCompatActivity(), KodeinAware {
    private val SPLASH_TIME_OUT:Long=1500 // 1.5 sec

    private lateinit var viewModel: ApplicationViewModel

    override val kodein by kodein()

    private val factory: ApplicationViewModelFactory by instance<ApplicationViewModelFactory>()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        makeFullScreen()
        setContentView(R.layout.activity_splash_screen)

        viewModel = ViewModelProviders.of(this, factory).get(ApplicationViewModel::class.java)
        viewModel.getApplicationDetails()
        viewModel.applicationDetails.observe(this, Observer {
            if (it.isActive == true) {
                iau_error_splash.visibility = View.GONE
//                toastMaker(it.toString())
                if (viewModel.isUpToDate.value != false) {
                    // This method will be executed once the timer is over
                    Handler().postDelayed({
                        // Start your app main activity
                        startActivity(Intent(this, MainActivity::class.java))
                        // close this activity
                        finish()
                    }, SPLASH_TIME_OUT)
                } else {
                    // Start your app main activity
                    startActivity(Intent(this, UpdateRequiredMainActivity::class.java))
                    // close this activity
                    finish()
                }
            } else {
                iau_error_splash.visibility = View.VISIBLE
            }
        })
        setFont()
    }

    override fun onResume() {
        super.onResume()

    }


    fun makeFullScreen() {
        if (Build.VERSION.SDK_INT < 16) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        } else {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
            actionBar?.hide()
        }
    }

    fun setFont() {
        var typeface = Typeface.createFromAsset(assets, "fonts/IranNastaliq.ttf")
        iau_title_splash.typeface = typeface
        typeface = Typeface.createFromAsset(assets, "fonts/Vazir-FD-WOL.ttf")
        iau_error_splash.typeface = typeface
    }

    fun toastMaker(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show()
    }
}
