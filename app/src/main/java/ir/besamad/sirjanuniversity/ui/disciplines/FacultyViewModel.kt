package ir.besamad.sirjanuniversity.ui.disciplines

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.besamad.sirjanuniversity.data.FacultyRepository
import ir.besamad.sirjanuniversity.data.models.Faculty
import ir.besamad.sirjanuniversity.utils.Coroutines
import java.lang.Exception

class FacultyViewModel(
    private val repo: FacultyRepository
): ViewModel() {

    private val _faculties = MutableLiveData<List<Faculty>>()

    val faculties: LiveData<List<Faculty>>
            get() = _faculties

    fun getFaculties(languageId: Int) {
        Coroutines.main {
            try {
                this._faculties.value = repo.getFaculties(languageId)
                return@main
            } catch (e: Exception) {

            }
        }
    }

}