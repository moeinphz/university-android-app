package ir.besamad.sirjanuniversity.ui.calendar

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.besamad.sirjanuniversity.data.AcademicCalendarRepository

class CalendarViewModelFactory(
    private val repository: AcademicCalendarRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CalendarViewModel(repository) as T
    }

}