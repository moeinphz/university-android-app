package ir.besamad.sirjanuniversity.ui.disciplines

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Department
import ir.besamad.sirjanuniversity.databinding.RecyclerviewDepartmentCellBinding

class DepartmentRecyclerAdapter(
    private val departmets: List<Department>,
    private val context: Context
): RecyclerView.Adapter<DepartmentRecyclerAdapter.DepartmentViewHolder>() {

    override fun getItemCount() = departmets.size


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DepartmentViewHolder(
            DataBindingUtil.inflate<RecyclerviewDepartmentCellBinding>(
                LayoutInflater.from(parent.context),
                R.layout.recyclerview_department_cell,
                parent,
                false
            )
        )


    override fun onBindViewHolder(holder: DepartmentViewHolder, position: Int) {
        holder.recyclerViewDepartmentCellBinding.department = departmets[position]
    }


    fun setTypeface(holder: DepartmentViewHolder) {
        val typeface = Typeface.createFromAsset(context.assets, "fonts/Vazir-Medium-FD-WOL.ttf")
    }


    inner class DepartmentViewHolder(
        val recyclerViewDepartmentCellBinding: RecyclerviewDepartmentCellBinding
    ): RecyclerView.ViewHolder(recyclerViewDepartmentCellBinding.root)

}