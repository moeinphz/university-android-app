package ir.besamad.sirjanuniversity.ui.disciplines

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.besamad.sirjanuniversity.data.FacultyRepository

class FacultyModelViewFactory(
    private val repository: FacultyRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return FacultyViewModel(repository) as T
    }
}