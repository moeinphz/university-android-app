package ir.besamad.sirjanuniversity.ui.announcementDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.besamad.sirjanuniversity.data.models.Announcement

class AnnouncementDetailViewModel : ViewModel() {

    private val _announcement = MutableLiveData<Announcement>()

    val announcement: LiveData<Announcement>
        get() = _announcement

    fun setAnnouncement(announcement: Announcement) {
        _announcement.value = announcement
    }
}
