package ir.besamad.sirjanuniversity.ui.disciplines

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.besamad.sirjanuniversity.data.DisciplineRepository

class DisciplineViewModelFactory(
    private val repository: DisciplineRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DisciplineViewModel(repository) as T
    }
}