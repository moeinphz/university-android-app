package ir.besamad.sirjanuniversity.ui.disciplineDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.besamad.sirjanuniversity.data.models.Discipline

class DisciplineDetailsViewModel: ViewModel() {

    private val _discipline = MutableLiveData<Discipline>()

    val discipline: LiveData<Discipline>
        get() = _discipline

    fun setDiscipline(discipline: Discipline) {
        _discipline.value = discipline
    }

}