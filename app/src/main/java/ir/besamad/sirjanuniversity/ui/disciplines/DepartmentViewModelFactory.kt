package ir.besamad.sirjanuniversity.ui.disciplines

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.besamad.sirjanuniversity.data.DepartmentRepository

class DepartmentViewModelFactory(
    private val repository: DepartmentRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DepartmentViewModel(repository) as T
    }
}