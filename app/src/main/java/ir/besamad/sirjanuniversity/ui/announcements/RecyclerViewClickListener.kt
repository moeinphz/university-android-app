package ir.besamad.sirjanuniversity.ui.announcements

import android.view.View
import ir.besamad.sirjanuniversity.data.models.Announcement

interface RecyclerViewClickListener {
    fun onRecyclerViewItemClick(view: View, announcement: Announcement)
}