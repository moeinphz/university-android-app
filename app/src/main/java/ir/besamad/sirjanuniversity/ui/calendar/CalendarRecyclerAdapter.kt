package ir.besamad.sirjanuniversity.ui.calendar

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.AcademicCalendar
import ir.besamad.sirjanuniversity.databinding.RecyclerviewAcademicCalendarCellBinding
import ir.besamad.sirjanuniversity.databinding.RecyclerviewAnnouncementCellBinding

class CalendarRecyclerAdapter(
    private val calendar: List<AcademicCalendar>,
    private val context: Context
): RecyclerView.Adapter<CalendarRecyclerAdapter.CalendarViewHolder>() {

    override fun getItemCount() = calendar.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CalendarViewHolder(
            DataBindingUtil.inflate<RecyclerviewAcademicCalendarCellBinding>(
                LayoutInflater.from(parent.context),
                R.layout.recyclerview_academic_calendar_cell,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: CalendarViewHolder, position: Int) {
        holder.recyclerviewAcademicCalendarCellBinding.calendar = calendar[position]

        val typeface = Typeface.createFromAsset(context.assets, "fonts/Vazir-Medium-FD-WOL.ttf")
        holder.recyclerviewAcademicCalendarCellBinding.textViewDateAcademicCalendar.typeface = typeface
        holder.recyclerviewAcademicCalendarCellBinding.textViewExpireDateAcademicCalendar.typeface = typeface
        holder.recyclerviewAcademicCalendarCellBinding.textViewTitleAcademicCalendar.typeface = typeface
    }

    inner class CalendarViewHolder(
        val recyclerviewAcademicCalendarCellBinding: RecyclerviewAcademicCalendarCellBinding
    ): RecyclerView.ViewHolder(recyclerviewAcademicCalendarCellBinding.root)

}