package ir.besamad.sirjanuniversity.ui.announcements

import android.content.Context
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.besamad.sirjanuniversity.data.AnnouncementsRepository
import ir.besamad.sirjanuniversity.data.models.Announcement
import ir.besamad.sirjanuniversity.data.network.SignalRListener
import ir.besamad.sirjanuniversity.utils.Coroutines
import ir.besamad.sirjanuniversity.utils.NetworkCallsResponses
import kotlinx.coroutines.Job
import java.io.IOException

class AnnouncementsViewModel(
    private val repository: AnnouncementsRepository
) : ViewModel() {

    var isLoading = MutableLiveData<Boolean>()

    lateinit var signalRListener: SignalRListener

    private val networkInterface: NetworkCallsResponses? = null
    private lateinit var job: Job
    private val _announcements = MutableLiveData<List<Announcement>>()

    val announcements: LiveData<List<Announcement>>
        get() = _announcements


    fun getAnnouncements() {
        isLoading.value = true
        Coroutines.main {
            try {
                _announcements.value = repository.getAnnouncements()
                isLoading.value = false
                return@main
            } catch (e: IOException) {
                networkInterface?.onFailure("Network Error")
            }
        }
    }


    fun receiveUpdatedData(context: Context) {
        signalRListener = SignalRListener.getInstance(context)

        if (_announcements.value != null) {
            signalRListener.announcementLiveData.observeForever {
                val a = this._announcements.value as ArrayList<Announcement>
                a.add(0, it)
                this._announcements.value = a
            }
        }

    }

    override fun onCleared() {
        super.onCleared()
        if (::job.isInitialized) job.cancel()
    }
}
