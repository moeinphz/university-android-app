package ir.besamad.sirjanuniversity.ui.announcements

import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Announcement
import ir.besamad.sirjanuniversity.databinding.AnnouncementsFragmentBinding
import ir.besamad.sirjanuniversity.utils.NetworkCallsResponses
import kotlinx.android.synthetic.main.announcements_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class AnnouncementsFragment : Fragment(), KodeinAware, RecyclerViewClickListener, NetworkCallsResponses {

    private lateinit var viewModel: AnnouncementsViewModel

    override val kodein by kodein()

    private val factory: AnnouncementsViewModelFactory by instance<AnnouncementsViewModelFactory>()

    var navController: NavController? = null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        return inflater.inflate(R.layout.announcements_fragment, container, false)
        val binding: AnnouncementsFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.announcements_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(AnnouncementsViewModel::class.java)

        viewModel.receiveUpdatedData(requireContext())
        viewModel.getAnnouncements()
        viewModel.announcements.observe(viewLifecycleOwner, Observer { announcements ->
            recyclerview_announcements.also {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter =
                    AnnouncementsRecyclerAdapter(
                        announcements.asReversed(),
                        requireContext(),
                        this
                    )
            }
        })

        binding.announcementViewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setFont()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onRecyclerViewItemClick(view: View, announcement: Announcement) {
        val bundle = bundleOf("announcement" to announcement)
        navController!!
            .navigate(R.id.action_announcementsFragment_to_announcementDetailFragment,
            bundle)
    }

    override fun onStarted() {

    }

    override fun onSuccess() {
    }

    override fun onFailure(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    fun setFont() {
        val typeface = Typeface.createFromAsset(requireContext().assets, "fonts/Vazir-FD-WOL.ttf")
        textView_announcementsListTitle_toolBar.typeface = typeface
    }

}
