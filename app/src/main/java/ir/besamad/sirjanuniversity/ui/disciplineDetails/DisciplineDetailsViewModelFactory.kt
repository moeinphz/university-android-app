package ir.besamad.sirjanuniversity.ui.disciplineDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DisciplineDetailsViewModelFactory: ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return DisciplineDetailsViewModel() as T
    }
}