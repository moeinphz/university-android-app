package ir.besamad.sirjanuniversity.ui.announcementDetails

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.besamad.sirjanuniversity.data.AnnouncementsRepository
import ir.besamad.sirjanuniversity.ui.announcements.AnnouncementsViewModel

class AnnouncementDetailsModelViewFactory (): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AnnouncementDetailViewModel() as T
    }

}