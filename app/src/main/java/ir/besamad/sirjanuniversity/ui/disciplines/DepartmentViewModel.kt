package ir.besamad.sirjanuniversity.ui.disciplines

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.besamad.sirjanuniversity.data.DepartmentRepository
import ir.besamad.sirjanuniversity.data.models.Department
import ir.besamad.sirjanuniversity.utils.Coroutines
import java.lang.Exception

class DepartmentViewModel(
    private val repo: DepartmentRepository
): ViewModel() {

    private  val _departments = MutableLiveData<List<Department>>()

    val departments: LiveData<List<Department>>
        get() = _departments

    fun getDepartments(langiageId: Int) {
        Coroutines.main {
            try {
                this._departments.value = repo.getDepartments(langiageId)
                return@main
            } catch (e: Exception) {

            }
        }
    }

}