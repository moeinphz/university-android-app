package ir.besamad.sirjanuniversity.ui

import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import ir.besamad.sirjanuniversity.R
import kotlinx.android.synthetic.main.activity_update_required_main.*

class UpdateRequiredMainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_required_main)

        setFont()
        selectIgnore()
        selectUpdate()

        linearLayout_actions_update.animation = getAnimation()
    }


    fun selectUpdate() {
        button_open_update_link.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse("http://www.iausirjan.ac.ir/ea/apps/")
            startActivity(openURL)
        }
    }

    fun selectIgnore() {
        textView_button_refuse_continue.setOnClickListener {
            // Start your app main activity
            startActivity(Intent(this, MainActivity::class.java))
            // close this activity
            finish()
        }
    }

    fun getAnimation(): Animation {
        return AnimationUtils.loadAnimation(this, R.anim.fade_in)
    }

    fun setFont() {
        var typeface = Typeface.createFromAsset(assets, "fonts/IranNastaliq.ttf")
        iau_title_update.typeface = typeface
        typeface = Typeface.createFromAsset(assets, "fonts/Vazir-FD-WOL.ttf")
        textView_update.typeface = typeface
        button_open_update_link.typeface = typeface
        textView_button_refuse_continue.typeface = typeface
    }
}