package ir.besamad.sirjanuniversity.ui.disciplines

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Discipline
import ir.besamad.sirjanuniversity.databinding.RecyclerviewDisciplineCellBinding

class DisciplineRecyclerAdapter(
    private val disciplines: List<Discipline>,
    private val context: Context,
    private val listener: RecyclerViewDisciplinesClicklistener
): RecyclerView.Adapter<DisciplineRecyclerAdapter.DisciplineViewHolder>() {

    override fun getItemCount() = disciplines.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DisciplineViewHolder(
            DataBindingUtil.inflate<RecyclerviewDisciplineCellBinding>(
                LayoutInflater.from(this.context),
                R.layout.recyclerview_discipline_cell,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: DisciplineViewHolder, position: Int) {
        holder.recyclerviewDisciplineCellBinding.discipline = disciplines[position]

        holder.recyclerviewDisciplineCellBinding.cardViewMainContainerDisciplineCell.startAnimation(getAnimation())
        setTypeface(holder)

        holder.recyclerviewDisciplineCellBinding.viewShadeVideoThumbnail.setOnClickListener {
            listener.onRecyclerViewItemClick(holder.recyclerviewDisciplineCellBinding.cardViewMainContainerDisciplineCell, disciplines[position])
        }
    }


    fun getTypeface(): Typeface {
        return Typeface.createFromAsset(context.assets, "fonts/Vazir-Medium-FD-WOL.ttf")
    }

    fun setTypeface(holder: DisciplineViewHolder) {
        holder.recyclerviewDisciplineCellBinding.textViewDeciplineTitle.typeface = getTypeface()
        holder.recyclerviewDisciplineCellBinding.textViewGraduationLevel.typeface = getTypeface()
    }

    fun getAnimation(): Animation {
        return AnimationUtils.loadAnimation(context, R.anim.fade_in)
    }

    inner class DisciplineViewHolder(
        val recyclerviewDisciplineCellBinding: RecyclerviewDisciplineCellBinding
    ): RecyclerView.ViewHolder(recyclerviewDisciplineCellBinding.root)

}