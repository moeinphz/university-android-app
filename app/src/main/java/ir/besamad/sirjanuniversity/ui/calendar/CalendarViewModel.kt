package ir.besamad.sirjanuniversity.ui.calendar

import android.view.View
import android.widget.Spinner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.besamad.sirjanuniversity.data.AcademicCalendarRepository
import ir.besamad.sirjanuniversity.data.models.AcademicCalendar
import ir.besamad.sirjanuniversity.utils.Coroutines
import java.io.IOException

class CalendarViewModel(
    private val repo: AcademicCalendarRepository
) : ViewModel() {

    private val _terms = MutableLiveData<List<String>>()
    private val _calendar = MutableLiveData<List<AcademicCalendar>>()

    val terms: LiveData<List<String>>
        get() = _terms

    val calendar: LiveData<List<AcademicCalendar>>
        get() = _calendar


    fun getTerms() {
        Coroutines.main {
            try {
                _terms.value = repo.getTerms()
                return@main
            } catch (e: IOException) {

            }
        }
    }

    fun getTheCalendar (term: String) {
        Coroutines.main {
            try {
                _calendar.value = repo.getCalendar(term)
                return@main
            } catch (e: IOException) {

            }
        }
    }
}
