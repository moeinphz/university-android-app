package ir.besamad.sirjanuniversity.ui.disciplines

import android.view.View
import ir.besamad.sirjanuniversity.data.models.Discipline

interface RecyclerViewDisciplinesClicklistener {
    fun onRecyclerViewItemClick(view: View, discipline: Discipline)
}