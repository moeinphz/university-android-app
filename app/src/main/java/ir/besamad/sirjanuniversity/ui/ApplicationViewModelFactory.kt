package ir.besamad.sirjanuniversity.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.besamad.sirjanuniversity.data.ApplicationDetailsRepository

class ApplicationViewModelFactory(
    private val repository: ApplicationDetailsRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ApplicationViewModel(
            repository
        ) as T
    }

}