package ir.besamad.sirjanuniversity.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.besamad.sirjanuniversity.data.ApplicationDetailsRepository
import ir.besamad.sirjanuniversity.data.models.ApplicationDetails
import ir.besamad.sirjanuniversity.utils.Coroutines
import java.lang.Exception

class ApplicationViewModel(
    private val repo: ApplicationDetailsRepository
): ViewModel() {

    var isLoading = MutableLiveData<Boolean>()

    var isUpToDate = MutableLiveData<Boolean>()
        get() = repo.isUpToDate

    private val _applicationDetails = MutableLiveData<ApplicationDetails>()

    val applicationDetails: LiveData<ApplicationDetails>
        get() = _applicationDetails

    fun getApplicationDetails() {
        Coroutines.main {
            try {
                _applicationDetails.value = repo.getApplicationDetails()
                isUpToDate.value = repo.isUpToDate.value!!
            } catch (e: Exception) {

            }
        }
    }

}