package ir.besamad.sirjanuniversity.ui.calendar

import android.graphics.Typeface
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.databinding.CalendarFragmentBinding
import kotlinx.android.synthetic.main.announcements_fragment.*
import kotlinx.android.synthetic.main.calendar_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import org.kodein.di.newInstance


class CalendarFragment : Fragment(), KodeinAware {

    private lateinit var viewModel: CalendarViewModel

    override val kodein by kodein()

    private val factory: CalendarViewModelFactory by instance<CalendarViewModelFactory>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: CalendarFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.calendar_fragment, container, false)
        viewModel = ViewModelProviders.of(this, factory).get(CalendarViewModel::class.java)
        viewModel.getTerms()

        binding.calendarViewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)


        viewModel.terms.observe(viewLifecycleOwner, Observer<List<String>> { terms->
            var adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, terms)
            spinnerTerms_Calendar.adapter = adapter
        })

        spinnerTerms_Calendar?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                viewModel.getTheCalendar(spinnerTerms_Calendar.selectedItem.toString())
            }
        }

        viewModel.calendar.observe(viewLifecycleOwner, Observer { theCalendar ->
            recyclerview_calendar.also {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter =
                    CalendarRecyclerAdapter(theCalendar, requireContext())
            }
        })

        setFont()

    }

    fun setFont() {
        val typeface = Typeface.createFromAsset(requireContext().assets, "fonts/Vazir-FD-WOL.ttf")
        textView_academicCalendarTitle_toolBar.typeface = typeface
    }

}
