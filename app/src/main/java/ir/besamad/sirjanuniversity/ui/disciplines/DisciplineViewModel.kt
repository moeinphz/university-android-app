package ir.besamad.sirjanuniversity.ui.disciplines

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.besamad.sirjanuniversity.data.DisciplineRepository
import ir.besamad.sirjanuniversity.data.models.Discipline
import ir.besamad.sirjanuniversity.utils.Coroutines

class DisciplineViewModel(
    private val repo: DisciplineRepository
): ViewModel() {


    var isOnSearch = MutableLiveData<Boolean>()
    var isLoading = MutableLiveData<Boolean>()


    private val _disciplines = MutableLiveData<List<Discipline>>()
    private val _disciplinesHelper = MutableLiveData<List<Discipline>>()

    val disciplines: LiveData<List<Discipline>>
        get() = _disciplines

    private val _discipline = MutableLiveData<Discipline>()

    val discipline: LiveData<Discipline>
        get() = _discipline


    fun getAllDisciplines(languageId: Int) {
        isLoading.value = true
        Coroutines.main {
            try {
                _disciplines.value = repo.getAllDisciplines(languageId)
                _disciplinesHelper.value = _disciplines.value
                isLoading.value = false
                return@main
            } catch (e: Exception) {

            }
        }
    }


    fun getDiscipline(id: Int) {
        Coroutines.main {
            try {
                _discipline.value = repo.getDisciplineById(id)
                return@main
            } catch (e: Exception) {

            }
        }
    }

    fun showSearch() {
        isOnSearch.value = true
    }

    fun hideSearch() {
        isOnSearch.value = false
    }

    fun filterDisciplines(text: CharSequence?) {
        if (text!!.isNotEmpty()){
            _disciplines.value =_disciplinesHelper.value?.filter {
                it.deciplineTitle.contains(text.toString())
            }
        } else {
            _disciplines.value = _disciplinesHelper.value
        }
    }

    fun filterBasedOnFacultyId(ids: MutableList<Int>, faucltiesSize: Int) {

        if (_disciplines.value != null) {
            if (ids.size == 0 || ids.size == faucltiesSize) {
                if (_disciplines.value!!.size != _disciplinesHelper.value!!.size) {
                    getAllDisciplines(1)
                }
            } else {
                var list: MutableList<Discipline> = ArrayList<Discipline>()
                for (id in ids) {
                    list.addAll(_disciplinesHelper.value!!.filter {
                        it.facultyId.equals(id)
                    })
                }
                _disciplines.value = list
            }
        }
    }
}