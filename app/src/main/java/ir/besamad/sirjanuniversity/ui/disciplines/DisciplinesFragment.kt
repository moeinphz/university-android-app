package ir.besamad.sirjanuniversity.ui.disciplines

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.core.view.marginRight
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Discipline
import ir.besamad.sirjanuniversity.databinding.DisciplinesFragmentBinding
import kotlinx.android.synthetic.main.disciplines_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.util.*

class DisciplineListFragment : Fragment(), KodeinAware, RecyclerViewDisciplinesClicklistener {

    interface OnDisciplineChosenListener {
        fun onDisciplineChosen(discipline: Discipline)
    }

    lateinit var onDisciplineChosen: OnDisciplineChosenListener

    private lateinit var disciplineViewModel: DisciplineViewModel
    private lateinit var facultyViewModel: FacultyViewModel

    override val kodein by kodein()

    private val disciplineFactory: DisciplineViewModelFactory by instance<DisciplineViewModelFactory>()
    private val facultyFactory: FacultyModelViewFactory by instance<FacultyModelViewFactory>()

    var filteringIdList: MutableList<Int> = ArrayList<Int>()


    //player bottomsheet
    var navController: NavController? = null



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: DisciplinesFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.disciplines_fragment, container, false)
        disciplineViewModel = ViewModelProviders.of(this, disciplineFactory).get(DisciplineViewModel::class.java)
        facultyViewModel = ViewModelProviders.of(this, facultyFactory).get(FacultyViewModel::class.java)

        disciplineViewModel.getAllDisciplines(1)
        facultyViewModel.getFaculties(1)

        onFacultiesObserve()

        binding.disciplineViewModel = disciplineViewModel
        binding.facultyViewModel = facultyViewModel

        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        onDisciplineObserve()

        setFont()

        toggleSearch()

        filterDisciplines()

        disciplineViewModel.isOnSearch.observe(viewLifecycleOwner, Observer {
            if (it == true) {
                this.openSoftKeyboard(requireContext(), editText_search)
            } else {
                this.hideSoftKeyboard(requireContext(), editText_search)
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onDisciplineChosen = (context) as OnDisciplineChosenListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)

    }


    fun setFont() {
        val typeface = getFont()
        textView_disciplinesListTitle_toolBar.typeface = typeface
//        textView_disciplines_departmentsTitle.typeface = typeface
        editText_search.typeface = typeface
    }

    fun getFont(): Typeface {
        return Typeface.createFromAsset(requireContext().assets, "fonts/Vazir-FD-WOL.ttf")
    }


    fun onFacultiesObserve() {
        facultyViewModel.faculties.observe(viewLifecycleOwner, Observer { faculties ->
            val inflater = LayoutInflater.from(context)
            for (fac in faculties){
                val chip_item = inflater.inflate(R.layout.chip_item, null, false) as Chip
                chip_item.text = fac.facultyTitle

                chip_item.typeface = getFont()
                chip_group_faculties.addView(chip_item)

                scrollView_faculties_disciplines.fullScroll(View.FOCUS_RIGHT)

                chip_item.setOnClickListener {
                    filterBasedOnFacultyId(fac.id)
                }
            }
        })
    }

    fun onDisciplineObserve() {
        disciplineViewModel.disciplines.observe(viewLifecycleOwner, Observer { disciplines ->
            recyclerview_disciplines.also {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                it.adapter =
                    DisciplineRecyclerAdapter(disciplines, requireContext(), this)
            }
        })
    }


    fun toggleSearch() {
        imageView_quit_search.setOnClickListener {
            disciplineViewModel.hideSearch()
        }
        imageView_search.setOnClickListener {
            if (disciplineViewModel.isLoading.value != true) {
                disciplineViewModel.showSearch()
            }
        }
    }

    fun openSoftKeyboard(context: Context, view: View) {
        view.requestFocus()
        // open the soft keyboard
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }

    fun hideSoftKeyboard(context: Context, view: View) {
        editText_search.setText("")
        view.clearFocus()
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.applicationWindowToken, 0)
    }

    // Filter
    fun filterDisciplines() {
        editText_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                disciplineViewModel.filterDisciplines(text)
            }

            override fun beforeTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
    }

    fun filterBasedOnFacultyId(id: Int) {
        if (filteringIdList.find { it -> id.equals(it) } == id) {
            filteringIdList.remove(id)
            disciplineViewModel.filterBasedOnFacultyId(filteringIdList, facultyViewModel.faculties.value!!.size)
        } else {
            filteringIdList.add(id)
            disciplineViewModel.filterBasedOnFacultyId(filteringIdList, facultyViewModel.faculties.value!!.size)
        }

    }

    // recyclerView Click
    override fun onRecyclerViewItemClick(view: View, discipline: Discipline) {
        view.startAnimation(getAnimationClickStart())
        view.startAnimation(getAnimationClickEnd())

//        val bundle = bundleOf("discipline" to discipline)
//
//        navController!!
//            .navigate(R.id.action_disciplineListFragment_to_disciplineDetailsFragment,
//                bundle)

        onDisciplineChosen.onDisciplineChosen(discipline)
    }


    // Animations
    fun getAnimationClickStart(): Animation {
        return AnimationUtils.loadAnimation(context, R.anim.ann_click_start)
    }

    fun getAnimationClickEnd(): Animation {
        return AnimationUtils.loadAnimation(context, R.anim.ann_click_end)
    }

//    private fun configureDetailsBottomSheet() {
//        // Get the fragment reference
//
//        val fragment = childFragmentManager.findFragmentById(R.id.filter_fragment)
//
//        fragment?.let {
//            // Get the BottomSheetBehavior from the fragment view
//            BottomSheetBehavior.from(it.requireView())?.let { bsb ->
//                // Set the initial state of the BottomSheetBehavior to HIDDEN
//                bsb.state = BottomSheetBehavior.STATE_HIDDEN
//
//                // Set the trigger that will expand your view
////                button.setOnClickListener { bsb.state = BottomSheetBehavior.STATE_EXPANDED }
//
//
//                // Set the reference into class attribute (will be used latter)
//                mBottomSheetBehavior = bsb
//            }
//        }
//    }

}
