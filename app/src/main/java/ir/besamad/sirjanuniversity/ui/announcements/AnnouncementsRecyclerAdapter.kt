package ir.besamad.sirjanuniversity.ui.announcements

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Announcement
import ir.besamad.sirjanuniversity.databinding.RecyclerviewAnnouncementCellBinding

class AnnouncementsRecyclerAdapter(
    private val announcements: List<Announcement>,
    private val context: Context,
    private val listener: RecyclerViewClickListener
): RecyclerView.Adapter<AnnouncementsRecyclerAdapter.AnnouncementsViewHolder>() {

    override fun getItemCount() = announcements.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        AnnouncementsViewHolder(
            DataBindingUtil.inflate<RecyclerviewAnnouncementCellBinding>(
                LayoutInflater.from(parent.context),
                R.layout.recyclerview_announcement_cell,
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: AnnouncementsViewHolder, position: Int) {
        holder.recyclerViewAnnouncementsBinding.announcement = announcements[position]
        val typeface = Typeface.createFromAsset(context.assets, "fonts/Vazir-Medium-FD-WOL.ttf")
        holder.recyclerViewAnnouncementsBinding.textviewAnnouncementMonth.typeface = typeface
        holder.recyclerViewAnnouncementsBinding.textviewAnnouncementMainTitle.typeface = typeface
        holder.recyclerViewAnnouncementsBinding.textviewAnnouncementLocation.typeface = typeface
        holder.recyclerViewAnnouncementsBinding.textviewAnnouncementTime.typeface = typeface


        holder.recyclerViewAnnouncementsBinding.root.setOnClickListener{
            listener.onRecyclerViewItemClick(holder.recyclerViewAnnouncementsBinding.root, announcements[position])
        }
    }

    inner class AnnouncementsViewHolder(
        val recyclerViewAnnouncementsBinding: RecyclerviewAnnouncementCellBinding
    ): RecyclerView.ViewHolder(recyclerViewAnnouncementsBinding.root)
}