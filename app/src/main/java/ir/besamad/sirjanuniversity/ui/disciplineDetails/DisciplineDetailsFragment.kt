package ir.besamad.sirjanuniversity.ui.disciplineDetails

import android.graphics.Typeface
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.models.Discipline
import ir.besamad.sirjanuniversity.databinding.DisciplineDetailsFragmentBinding
import kotlinx.android.synthetic.main.discipline_details_fragment.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class DisciplineDetailsFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val factory: DisciplineDetailsViewModelFactory by instance<DisciplineDetailsViewModelFactory>()
    private lateinit var viewModel: DisciplineDetailsViewModel

    lateinit var discipline: Discipline

    var playbackPosition = 0
    private var rtspUrl = ""
    private lateinit var mediaController: MediaController



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        discipline = requireArguments().getParcelable<Discipline>("discipline")!!

//        discipline.helpDescription = stripHtml(discipline.helpDescription)
//        discipline.videoDescription = stripHtml(discipline.videoDescription)

        rtspUrl = "http://10.0.2.2:5000/" + discipline.videoPath
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: DisciplineDetailsFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.discipline_details_fragment, container, false)

        viewModel = ViewModelProviders.of(this, factory).get(DisciplineDetailsViewModel::class.java)
        viewModel.setDiscipline(discipline)
        binding.discipline = viewModel.discipline.value
        binding.lifecycleOwner = this

        return binding.root
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        mediaController = MediaController(requireContext())
        mediaController.setAnchorView(cardView_videoView_container)
        videoView.setMediaController(mediaController)
        val uri = Uri.parse(rtspUrl)
        videoView.setVideoURI(uri)
        videoView.seekTo(playbackPosition)
        videoView.start()

        videoView.setOnPreparedListener {

        }

        videoView.setOnInfoListener { player, what, extras ->
            if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                loadingProgressSpinner.visibility = View.INVISIBLE
            }
            true
        }

        clickBack()
    }

    override fun onStart() {
        super.onStart()
        loadingProgressSpinner.visibility = View.VISIBLE

    }

    override fun onPause() {
        super.onPause()
        videoView.pause()
        playbackPosition = videoView.currentPosition
    }

    override fun onStop() {
        videoView.stopPlayback()
        super.onStop()
    }


//    fun stripHtml(html: String?): String {
//        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString()
//        } else {
//            Html.fromHtml(html).toString()
//        }
//    }


    fun getFont(): Typeface {
        return Typeface.createFromAsset(requireContext().assets, "fonts/Vazir-FD-WOL.ttf")
    }

    fun setFont() {
        val typeface = getFont()
        textView_departmentTitle.typeface = typeface
        textView_description.typeface = typeface
        textView_description2.typeface = typeface
        textView_disciplineTitle.typeface = typeface
    }


    fun clickBack() {
        imageView_quit_disciplineClose.setOnClickListener {
            requireActivity().onBackPressed()

        }
    }
}
