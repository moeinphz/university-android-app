package ir.besamad.sirjanuniversity.ui.announcements

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.besamad.sirjanuniversity.data.AnnouncementsRepository

@Suppress("UNCHECKED_CAST")
class AnnouncementsViewModelFactory(
    private val repository: AnnouncementsRepository
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AnnouncementsViewModel(
            repository
        ) as T
    }

}