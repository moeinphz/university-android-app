package ir.besamad.sirjanuniversity

import android.app.Application
import ir.besamad.mvvmlongertutorial.data.network.NetworkConnectionInterceptor
import ir.besamad.sirjanuniversity.data.*
import ir.besamad.sirjanuniversity.data.network.*
import ir.besamad.sirjanuniversity.data.preferences.PreferenceProvider
import ir.besamad.sirjanuniversity.ui.ApplicationViewModelFactory
import ir.besamad.sirjanuniversity.ui.announcementDetails.AnnouncementDetailsModelViewFactory
import ir.besamad.sirjanuniversity.ui.announcements.AnnouncementsViewModelFactory
import ir.besamad.sirjanuniversity.ui.calendar.CalendarViewModelFactory
import ir.besamad.sirjanuniversity.ui.disciplineDetails.DisciplineDetailsViewModelFactory
import ir.besamad.sirjanuniversity.ui.disciplines.DepartmentViewModelFactory
import ir.besamad.sirjanuniversity.ui.disciplines.DisciplineViewModelFactory
import ir.besamad.sirjanuniversity.ui.disciplines.FacultyModelViewFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class SirjanUniversity: Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@SirjanUniversity))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }

        bind() from singleton { PreferenceProvider(instance()) }

        bind() from singleton { AnnouncementsApi(instance()) }

        bind() from singleton { AnnouncementsRepository(instance()) }

        bind() from provider { AnnouncementsViewModelFactory(instance()) }

        bind() from provider { AnnouncementDetailsModelViewFactory() }


        // Academic Calendar
        bind() from singleton { CalendarApi(instance()) }

        bind() from singleton { AcademicCalendarRepository(instance()) }

        bind() from provider { CalendarViewModelFactory(instance()) }

        // Desciplines
        bind() from  singleton { DisciplineApi(instance()) }

        bind() from singleton { DisciplineRepository(instance()) }

        bind() from provider { DisciplineViewModelFactory(instance()) }

        bind() from provider { DisciplineDetailsViewModelFactory() }

        // Departments
        bind() from singleton { DepartmentApi(instance()) }

        bind() from singleton { DepartmentRepository(instance()) }

        bind() from provider { DepartmentViewModelFactory(instance()) }

        // Faculties
        bind() from singleton { FacultyApi(instance()) }

        bind() from singleton { FacultyRepository(instance()) }

        bind() from provider { FacultyModelViewFactory(instance()) }

        // Application Details
        bind() from singleton { ApplicationApi(instance()) }

        bind() from singleton { ApplicationDetailsRepository(instance(), instance()) }

        bind() from provider { ApplicationViewModelFactory(instance()) }
    }

}