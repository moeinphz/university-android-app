package ir.besamad.sirjanuniversity.data.models

import com.google.gson.annotations.SerializedName

data class Faculty(
    @SerializedName("id")
    val id: Int,
    @SerializedName("facultyPhotoPath")
    val facultyPhotoPath: String,
    @SerializedName("facultyPresidentUserId")
    val facultyPresidentUserId: Int,
    @SerializedName("facultyPresidentName")
    val facultyPresidentName: String,
    @SerializedName("facultyVicePresidentUserId")
    val facultyVicePresidentUserId: Int,
    @SerializedName("facultyVicePresidentName")
    val facultyVicePresidentName: String,
    @SerializedName("facultyContactNumber")
    val facultyContactNumber: String,
    @SerializedName("facultyFaxNumber")
    val facultyFaxNumber: String,
    @SerializedName("facultyTitle")
    val facultyTitle: String,
    @SerializedName("facultyDescription")
    val facultyDescription: String
)