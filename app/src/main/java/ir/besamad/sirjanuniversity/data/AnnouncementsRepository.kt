package ir.besamad.sirjanuniversity.data

import ir.besamad.sirjanuniversity.data.models.Announcement
import ir.besamad.sirjanuniversity.data.network.AnnouncementsApi
import ir.besamad.sirjanuniversity.data.network.SafeApiRequest

class AnnouncementsRepository(
    private val api: AnnouncementsApi
): SafeApiRequest() {

    suspend fun getAnnouncements() = apiRequest { api.getAnnouncements() } as ArrayList<Announcement>


}