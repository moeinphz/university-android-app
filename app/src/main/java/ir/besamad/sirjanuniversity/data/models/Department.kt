package ir.besamad.sirjanuniversity.data.models

import com.google.gson.annotations.SerializedName


data class Department(
    @SerializedName("id")
    val id: Int,
    @SerializedName("managerUserId")
    val managerUserId: Int,
    @SerializedName("managerProfilePhoto")
    val managerProfilePhoto: String,
    @SerializedName("managerName")
    val managerName: String,
    @SerializedName("facultyId")
    val facultyId: Int,
    @SerializedName("facultyTitle")
    val facultyTitle: String,
    @SerializedName("departmentTitle")
    val departmentTitle: String
)