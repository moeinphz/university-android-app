package ir.besamad.sirjanuniversity.data.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import ir.besamad.sirjanuniversity.R
import ir.besamad.sirjanuniversity.data.network.SignalRListener
import ir.besamad.sirjanuniversity.ui.MainActivity


open class ForegroundService: Service() {

    private val CHANNEL_ID = "SirjanUniversityService"
    val t = Thread()
//    lateinit var hubConnection: HubConnection
    lateinit var signalRListener: SignalRListener
//
//    lateinit var notificationManager: NotificationManager
//    lateinit var notificationChannel: NotificationChannel
//    lateinit var notificationBuilder: Notification.Builder
//    private val channel_id: "ir.besamad.sirjanuniversity.data.service"


    companion object {
        fun startService(context: Context, message: String) {
            val startIntent = Intent(context, ForegroundService::class.java)
            startIntent.putExtra("inputExtra", message)
            ContextCompat.startForegroundService(context, startIntent)
        }
        fun stopService(context: Context) {
            val stopIntent = Intent(context, ForegroundService::class.java)
            context.stopService(stopIntent)
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        //do heavy work on a background thread
        val context = this
        t.run {
            signalRListener = SignalRListener.getInstance(context)
            signalRListener.startConnection()
            signalRListener.onListening(context)
        }

//        Toast.makeText(this, "Hi", Toast.LENGTH_LONG).show()

        val input = intent?.getStringExtra("inputExtra")
        createNotificationChannel()
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Azad University of Sirjan")
            .setContentText(input)
            .setSmallIcon(R.drawable.iau_logo)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(1, notification)
        //stopSelf();
        return START_STICKY_COMPATIBILITY
    }
    override fun onBind(intent: Intent): IBinder? {
        return null
    }
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    fun onSignalCall() {
//        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

//        val intent = Intent(this, LauncherActivity::class.java)
//        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            notificationChannel = NotificationChannel("ir.besamad.sirjanuniversity.data.service", "Description", NotificationManager.IMPORTANCE_HIGH)
//            notificationManager.createNotificationChannel(notificationChannel)
//
//            notificationBuilder = Notification.Builder(this, "ir.besamad.sirjanuniversity.data.service")
//                .setContentTitle("New Notification")
//                .setSmallIcon(R.drawable.ic_date_range_black)
//                .setContentIntent(pendingIntent)
//        } else {
//            notificationBuilder = Notification.Builder(this)
//                .setContentTitle("New Notification")
//                .setSmallIcon(R.drawable.ic_date_range_black)
//                .setContentIntent(pendingIntent)
//        }
//        notificationManager.notify(1234, notificationBuilder.build())
    }

}