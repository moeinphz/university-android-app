package ir.besamad.sirjanuniversity.data

import ir.besamad.sirjanuniversity.data.models.AcademicCalendar
import ir.besamad.sirjanuniversity.data.network.CalendarApi
import ir.besamad.sirjanuniversity.data.network.SafeApiRequest

class AcademicCalendarRepository(
    private val api: CalendarApi
): SafeApiRequest() {

    suspend fun getTerms() = apiRequest { api.getTerms() } as ArrayList<String>

    suspend fun getCalendar(term: String) = apiRequest { api.getCalendar(term) } as ArrayList<AcademicCalendar>

}