package ir.besamad.sirjanuniversity.data.models

import com.google.gson.annotations.SerializedName


data class AcademicCalendar(
    @SerializedName("description")
    val description: String,
    @SerializedName("eventDate")
    val eventDate: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("term")
    val term: String,
    @SerializedName("title")
    val title: String
)