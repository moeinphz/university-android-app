package ir.besamad.sirjanuniversity.data

import ir.besamad.sirjanuniversity.data.models.Discipline
import ir.besamad.sirjanuniversity.data.network.DisciplineApi
import ir.besamad.sirjanuniversity.data.network.SafeApiRequest

class DisciplineRepository(
    private val api: DisciplineApi
): SafeApiRequest() {

    suspend fun getAllDisciplines(languageId: Int) = apiRequest { api.getAllDisciplines(languageId) } as ArrayList<Discipline>

    suspend fun getDisciplineById(id: Int) = apiRequest { api.getDisciplineById(id) } as Discipline
}