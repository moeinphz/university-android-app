package ir.besamad.sirjanuniversity.data.models

import com.google.gson.annotations.SerializedName

data class ApplicationDetails(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("isActive")
    val isActive: Boolean,
    @SerializedName("version")
    val version: Float
)