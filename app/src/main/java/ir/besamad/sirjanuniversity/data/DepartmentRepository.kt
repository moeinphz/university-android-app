package ir.besamad.sirjanuniversity.data

import ir.besamad.sirjanuniversity.data.models.Department
import ir.besamad.sirjanuniversity.data.network.DepartmentApi
import ir.besamad.sirjanuniversity.data.network.SafeApiRequest

class DepartmentRepository(
    private val api: DepartmentApi
): SafeApiRequest() {

    suspend fun getDepartments(languageId: Int) = apiRequest { api.getDepartments(languageId) } as ArrayList<Department>

}