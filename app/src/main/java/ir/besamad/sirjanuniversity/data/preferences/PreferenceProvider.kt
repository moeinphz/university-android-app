package ir.besamad.sirjanuniversity.data.preferences

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

private val KEY_VERSION = "key_version"

class PreferenceProvider(
    private val context: Context
) {

    val appContext = context.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)


    fun saveVersion(version: Float) {
        preference.edit().putFloat(
            KEY_VERSION,
            version
        ).apply()
    }

    fun getVersion(): Float {
        return preference.getFloat(KEY_VERSION, 0f)
    }

}