package ir.besamad.sirjanuniversity.data

import ir.besamad.sirjanuniversity.data.models.Faculty
import ir.besamad.sirjanuniversity.data.network.FacultyApi
import ir.besamad.sirjanuniversity.data.network.SafeApiRequest

class FacultyRepository(
    private val api: FacultyApi
): SafeApiRequest() {

    suspend fun getFaculties(languageId: Int) = apiRequest { api.getFaculties(languageId) } as ArrayList<Faculty>
}