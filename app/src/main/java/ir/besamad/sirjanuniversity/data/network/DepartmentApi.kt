package ir.besamad.sirjanuniversity.data.network

import ir.besamad.mvvmlongertutorial.data.network.NetworkConnectionInterceptor
import ir.besamad.sirjanuniversity.data.models.Department
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface DepartmentApi {

    @GET("{languageId}")
    suspend fun getDepartments(@Path("languageId") languageId: Int): Response<List<Department>>




    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): DepartmentApi {
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://10.0.2.2:5000/api/Department/")
                .build()
                .create(DepartmentApi::class.java)
        }
    }
}