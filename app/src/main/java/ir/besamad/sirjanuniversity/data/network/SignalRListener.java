package ir.besamad.sirjanuniversity.data.network;

import android.app.LauncherActivity;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;


import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;
import com.microsoft.signalr.HubConnectionState;

import ir.besamad.sirjanuniversity.R;
import ir.besamad.sirjanuniversity.data.models.Announcement;

public class SignalRListener {
    private static SignalRListener instance;

    private HubConnection hubConnection;

    private NotificationManager notificationManager;
    private NotificationChannel notificationChannel;
    private Notification.Builder notificationBuilder;

//    public static HubConnection hubConnection;
    public Announcement announcement;

    public MutableLiveData<Announcement> announcementLiveData = new MutableLiveData<>();


    public static SignalRListener getInstance(Context context) {
        if (instance == null) {
            instance = new SignalRListener(context);
        }
        return instance;
    }

    private SignalRListener(Context context) {
        onListening(context);
    }

    public void onListening(Context context) {
        hubConnection = HubConnectionBuilder.create("http://10.0.2.2:5000/notification").build();

        hubConnection.on("AnnouncementNotification", (AnnouncementToReturn, isNew) -> {
            this.announcement = AnnouncementToReturn;

//            Toast.makeText(context, "new", Toast.LENGTH_LONG).show();

            this.announcementLiveData.postValue(AnnouncementToReturn);

            notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent intent = new Intent(context, LauncherActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                notificationChannel = new NotificationChannel("ir.besamad.sirjanuniversity.data.service", "Description", NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(notificationChannel);

                notificationBuilder = new Notification.Builder(context, "ir.besamad.sirjanuniversity.data.service")
                        .setContentTitle("اطلاعیه دانشگاه آزاد اسلامی واحد سیرجان")
                        .setContentText(AnnouncementToReturn.getAnnouncementTitle())
                        .setSmallIcon(R.drawable.iau_logo)
                        .setContentIntent(pendingIntent);
            } else {
                notificationBuilder = new Notification.Builder(context)
                        .setContentTitle("اطلاعیه دانشگاه آزاد اسلامی واحد سیرجان")
                        .setContentText(AnnouncementToReturn.getAnnouncementTitle())
                        .setSmallIcon(R.drawable.iau_logo)
                        .setContentIntent(pendingIntent);
            }
            notificationManager.notify(1234, notificationBuilder.build());

        }, Announcement.class, Boolean.class);
    }


    public boolean startConnection() {
        if (hubConnection.getConnectionState() == HubConnectionState.DISCONNECTED) {
            hubConnection.start();
            return true;
        } else { return false; }
    }

    public boolean stopConnection() {
        if (hubConnection.getConnectionState() == HubConnectionState.CONNECTED) {
            hubConnection.stop();
            return true;
        } else { return false; }
    }


    public void sendToServer(float x, float y) {
        if (hubConnection.getConnectionState() == HubConnectionState.CONNECTED) {
            hubConnection.send("MoveViewFromServer", x, y);
        }
    }
}
