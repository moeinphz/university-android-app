package ir.besamad.sirjanuniversity.data

import androidx.lifecycle.MutableLiveData
import ir.besamad.sirjanuniversity.data.models.ApplicationDetails
import ir.besamad.sirjanuniversity.data.network.ApplicationApi
import ir.besamad.sirjanuniversity.data.network.SafeApiRequest
import ir.besamad.sirjanuniversity.data.preferences.PreferenceProvider

class ApplicationDetailsRepository(
    private val api: ApplicationApi,
    private val pref: PreferenceProvider
): SafeApiRequest() {

    var isUpToDate = MutableLiveData<Boolean>()

    suspend fun getApplicationDetails(): ApplicationDetails {
        val app = apiRequest { api.getApplicationDetails() }
        handleVersion(app)
        return app
    }

    fun handleVersion(applicationDetails: ApplicationDetails) {
        if (pref.getVersion() == 0f) {
            pref.saveVersion(applicationDetails.version)
            isUpToDate.value = true
        } else {
            if (pref.getVersion() != applicationDetails.version) {
                isUpToDate.value = false
            }
        }
    }

}