package ir.besamad.sirjanuniversity.data.network

import ir.besamad.mvvmlongertutorial.data.network.NetworkConnectionInterceptor
import ir.besamad.sirjanuniversity.data.models.ApplicationDetails
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ApplicationApi {

//    @GET("2")
    @GET("1")
    suspend fun getApplicationDetails(): Response<ApplicationDetails>//because it is a network call we make it suspend

    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): ApplicationApi {
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://10.0.2.2:5000/api/ApplicationDetails/")
                .build()
                .create(ApplicationApi::class.java)
        }
    }

}