package ir.besamad.sirjanuniversity.data.models

import android.os.Parcelable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Discipline(
    @SerializedName("id")
    val id: Int,
    @SerializedName("deparmentId")
    val deparmentId: Int,
    @SerializedName("departmentTitle")
    val departmentTitle: String,
    @SerializedName("deciplineTitle")
    val deciplineTitle: String,
    @SerializedName("helpDescription")
    var helpDescription: String,
    @SerializedName("graduationLevel")
    val graduationLevel: String,
    @SerializedName("videoTitle")
    val videoTitle: String,
    @SerializedName("videoDescription")
    var videoDescription: String,
    @SerializedName("videoType")
    val videoType: String,
    @SerializedName("videoPath")
    var videoPath: String,
    @SerializedName("facultyId")
    val facultyId: Int
): Parcelable


