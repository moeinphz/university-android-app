package ir.besamad.sirjanuniversity.data.network

import ir.besamad.mvvmlongertutorial.data.network.NetworkConnectionInterceptor
import ir.besamad.sirjanuniversity.data.models.Discipline
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface DisciplineApi {

    @GET("AllWithVideo/{languageId}")
    suspend fun getAllDisciplines(@Path("languageId") languageId: Int): Response<List<Discipline>>


    @GET("forlist/1/{id}")
    suspend fun getDisciplineById(@Path("id") id: Int): Response<Discipline>



    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): DisciplineApi {
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()


            return Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://10.0.2.2:5000/api/Decipline/")
                .build()
                .create(DisciplineApi::class.java)
        }
    }
}