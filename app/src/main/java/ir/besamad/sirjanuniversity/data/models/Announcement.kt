package ir.besamad.sirjanuniversity.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Announcement(
    @SerializedName("announcementContent")
    val announcementContent: String,
    @SerializedName("announcementTime")
    val announcementTime: String,
    @SerializedName("announcementTitle")
    val announcementTitle: String,
    @SerializedName("categoryId")
    val categoryId: Int,
    @SerializedName("departmentId")
    val departmentId: Int,
    @SerializedName("expireDate")
    val expireDate: String,
    @SerializedName("filePath")
    val filePath: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("location")
    val location: String,
    @SerializedName("positionId")
    val positionId: Int,
    @SerializedName("postedDate")
    val postedDate: String,
    @SerializedName("userId")
    val userId: Int
): Parcelable