package ir.besamad.mvvmlongertutorial.data.network

import android.content.Context
import android.net.ConnectivityManager
import ir.besamad.sirjanuniversity.utils.NoInternetException
import okhttp3.Interceptor
import okhttp3.Response

class NetworkConnectionInterceptor(
    private val context: Context
): Interceptor {

    private val applicationContext = context.applicationContext

    override fun intercept(chain: Interceptor.Chain): Response {
        // This function will intercept our network calls
        if (!isInternetAvailable()) {
            throw NoInternetException("Make sure you have an Active Data Connection")
        }
        return chain.proceed(chain.request())
    }

    private fun isInternetAvailable(): Boolean {
        // To check our connection to the internet
        val connectivityManager =
            applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connectivityManager.activeNetworkInfo.also {
            return it != null && it.isConnected
        }
    }
}