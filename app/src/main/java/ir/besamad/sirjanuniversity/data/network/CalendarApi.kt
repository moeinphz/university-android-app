package ir.besamad.sirjanuniversity.data.network

import ir.besamad.mvvmlongertutorial.data.network.NetworkConnectionInterceptor
import ir.besamad.sirjanuniversity.data.models.AcademicCalendar
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface CalendarApi {



    @GET("terms")
    suspend fun getTerms(): Response<List<String>>

    @GET("term/1/{term}")
    suspend fun getCalendar(@Path("term") term: String): Response<List<AcademicCalendar>>

    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ): CalendarApi {
            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://10.0.2.2:5000/api/Calendar/")
                .build()
                .create(CalendarApi::class.java)
        }
    }
}