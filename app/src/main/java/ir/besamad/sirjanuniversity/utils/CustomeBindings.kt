package ir.besamad.sirjanuniversity.utils

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import ir.besamad.sirjanuniversity.R


@BindingAdapter("imageBindingAdapter")
fun loadImage(view: ImageView, url: String) {
    Glide.with(view.context)
        .asBitmap()
        .load("http://10.0.2.2:5000/" + url)
        .apply(
            RequestOptions()
            .placeholder(R.drawable.field_placeholder2)
                .error(R.drawable.field_placeholder2)
        )
        .into(view)


}

