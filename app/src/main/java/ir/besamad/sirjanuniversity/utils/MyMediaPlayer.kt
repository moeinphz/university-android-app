package ir.besamad.sirjanuniversity.utils

import android.content.Context
import android.media.MediaPlayer
import android.net.Uri
import android.view.View
import android.widget.MediaController
import android.widget.ProgressBar
import android.widget.VideoView
import androidx.appcompat.app.AppCompatActivity

class MyMediaPlayer (val videoView: VideoView,
                   val context: Context,
                   val progressBar: ProgressBar,
                   val anchor: View,
                   val url: String) {

    private var playbackPosition = 0
    private val rtspUrl = "http://10.0.2.2:5000/" + url
    private lateinit var mediaController: MediaController

    init {
        mediaController = MediaController(context)

        videoView.setOnPreparedListener {
            mediaController.setAnchorView(anchor)
            videoView.setMediaController(mediaController)
            videoView.seekTo(playbackPosition)
            videoView.start()
        }

        videoView.setOnInfoListener { player, what, extras ->
            if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                progressBar.visibility = View.INVISIBLE
            }
            true
        }

    }

    fun onStart() {
        progressBar.visibility = View.VISIBLE
        val uri = Uri.parse(rtspUrl)
        videoView.setVideoURI(uri)
    }


    fun onPause() {
        videoView.pause()
        playbackPosition = videoView.currentPosition
    }

    fun onStop() {
        videoView.stopPlayback()
    }

}