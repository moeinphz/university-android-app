package ir.besamad.sirjanuniversity.utils

import java.io.IOException

class ApiExcetion(message: String): IOException(message)
class NoInternetException(message: String): IOException(message)