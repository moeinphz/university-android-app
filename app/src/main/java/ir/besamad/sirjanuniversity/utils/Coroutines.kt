package ir.besamad.sirjanuniversity.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

// functions for Coroutines to make suspend functions and pass data through
// Object so that we can directly call the functions
object Coroutines {
    fun main(work: suspend (() -> Unit)) =
        CoroutineScope(Dispatchers.Main).launch {
            work()
        } // Higher order functions and lamda in kotlin

    fun io(work: suspend (() -> Unit)) =
        CoroutineScope(Dispatchers.IO).launch {
            work()
        } // Higher order functions and lamda in kotlin


}