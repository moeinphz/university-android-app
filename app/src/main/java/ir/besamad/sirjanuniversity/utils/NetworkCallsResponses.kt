package ir.besamad.sirjanuniversity.utils

interface NetworkCallsResponses {
    fun onStarted()
    fun onSuccess()
    fun onFailure(message: String)
}